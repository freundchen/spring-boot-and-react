package com.example.demo.usersettings.repository

import com.example.demo.usersettings.model.UserSettings
import org.assertj.core.api.KotlinAssertions
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@DataJpaTest
class UserSettingsRepositoryIT {

    @Autowired
    lateinit var serviceUnderTest: UserSettingsRepository

    @Before
    fun setUp() {
        serviceUnderTest.deleteAll()
    }

    @Test
    fun canSaveUserSettings() {
        // given
        val aValidUserId = "95c4e9ad-c757-4e16-a64c-7738ed0a4f37"
        val userSetting = aUserSetting(aValidUserId)

        // when
        serviceUnderTest.save(userSetting)

        // then
        KotlinAssertions.assertThat(serviceUnderTest.findAll().first()).extracting("userId").contains(aValidUserId)
    }

    @Test
    fun canFindUserSettingsReturnsEmptyByDefault() {
        // when
        val result = serviceUnderTest.findByUserId("doesnotExist")
        // then
        KotlinAssertions.assertThat(result).isNull()
    }

    @Test
    fun canFindUserSettingsByUserId() {
        // given
        val aValidUserId = "95c4e9ad-c757-4e16-a64c-7738ed0a4f37"

        // when
        val userSetting = serviceUnderTest.save(aUserSetting(aValidUserId))

        // then
        KotlinAssertions.assertThat(serviceUnderTest.findByUserId(aValidUserId)).isEqualTo(userSetting)
    }

    private fun aUserSetting(userId: String): UserSettings {
        return UserSettings.createSettings(userId)
    }
}