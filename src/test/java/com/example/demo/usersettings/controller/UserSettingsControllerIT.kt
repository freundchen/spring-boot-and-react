package com.example.demo.usersettings.controller

import io.restassured.module.mockmvc.RestAssuredMockMvc
import io.restassured.module.mockmvc.specification.MockMvcRequestSpecification
import org.hamcrest.Matchers
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.context.WebApplicationContext

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
class UserSettingsControllerIT {

    @Autowired
    lateinit var context: WebApplicationContext

    fun givenContext() = RestAssuredMockMvc.given().webAppContextSetup(context)

    @Test
    fun `can get user-settings for certain user`() {
        val aValidUserId = "7f974cb2-152d-4c8f-9512-08f772fdefed"

        givenContext()
                .whenever().get("/api/user/$aValidUserId").then()
                .statusCode(200)
                .contentType("application/json")
                .body("id", Matchers.equalTo(9999999),
                        "userId", Matchers.equalTo("7f974cb2-152d-4c8f-9512-08f772fdefed"),
                        "layout", Matchers.equalTo("tiles"),
                        "sort", Matchers.equalTo("ADDED_ON")
                )
    }

    private fun MockMvcRequestSpecification.whenever() = `when`()
}