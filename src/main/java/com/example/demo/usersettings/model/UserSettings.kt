package com.example.demo.usersettings.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
class UserSettings private constructor(

    var userId: String?,

    var layout: String?,

    var sort: String?
) {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private var id: Long? = null

    companion object {
        fun createSettings(userId: String) = UserSettings(userId = userId, layout = Layout.TILES.value, sort = Sort.ADDED_ON.name)
    }
}

enum class Layout(val value: String) {
    LIST("list"),
    TILES("tiles"),
    SHUFFLE("shuffle")
}

enum class Sort {
    ADDED_ON,
    ALPHABETICALLY,
    PRICE_ASCENDING,
    PRICE_DESCENDING,
    CATEGORY,
    PRICE_DROP_TOTAL,
    PRICE_DROP_PERCENTAL
}
