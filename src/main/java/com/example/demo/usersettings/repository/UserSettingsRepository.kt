package com.example.demo.usersettings.repository

import com.example.demo.usersettings.model.UserSettings
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface UserSettingsRepository : JpaRepository<UserSettings, Long> {
    @Query("FROM UserSettings u WHERE u.userId = ?1")
    fun findByUserId(userId: String): UserSettings?
}