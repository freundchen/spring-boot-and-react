package com.example.demo.usersettings.controller

import com.example.demo.usersettings.model.UserSettings
import com.example.demo.usersettings.repository.UserSettingsRepository
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class UserSettingsController(
        private val userSettingsRepository: UserSettingsRepository
) {

    private val logger = KotlinLogging.logger { }


    @GetMapping("/user/{userId}")
    fun getUserSettingsByUserId(@PathVariable(value = "userId") userId: String): ResponseEntity<UserSettings> {
        logger.error { "get user-settings called for user-id: $userId" }
        val result = userSettingsRepository.findByUserId(userId)
        if (result != null) {
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .body(result)
        }
        logger.error { "no user-settings found for user-id: $userId" }
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .build()
    }
}

