import React from "react";
import {FontAwesomeIcon as IconLib} from "@fortawesome/react-fontawesome";
import {StyleSheet, css} from 'aphrodite';
import "../../../../scss/components/settingssection/layoutswitch/LayoutSwitch.scss";

const SortItems = ({sort, onSortChange}) => {

    return (
        <div className="LayoutSwitch">
            <span className={sort}>
                <IconLib
                    icon="clock"
                    onClick={() => onSortChange("date")}
                />
                <IconLib
                    icon="euro-sign"
                    onClick={() => onSortChange("price-up")}
                />
                <IconLib
                    icon="sort-alpha-down"
                    onClick={() => onSortChange("title-alpha-down")}
                />
                <IconLib
                    icon="sort-alpha-up"
                    onClick={() => onSortChange("title-alpha-up")}
                />
                <IconLib
                    icon="fire"
                    onClick={() => onSortChange("bargains")}
                />
                <IconLib
                    icon="sitemap"
                    onClick={() => onSortChange("category")}
                />
            </span>
        </div>
    )
};

export default SortItems;

// TODO: how to style cascading with aphrodite?
const styles = StyleSheet.create({
    layoutSwitch: {
        fontSize: '26px',
        color: 'darkgrey',
        icon: {
            marginLeft: '10px',
            cursor: 'pointer',
            ':hover': {
                color: 'cornflowerblue',
                transform: 'scale(1.1)',
            }
        },
    },
});

export const sorted = (sort, wishlist) => {

    switch (sort) {
        case "title-alpha-down":
            return sortAlphaDown(wishlist);
        case "title-alpha-up":
            return sortAlphaUp(wishlist);
        case "bargains":
            return sortBargainsUp(wishlist);
        case "category":
            return sortByCategory(wishlist);
        case "price-up":
            return sortByPriceUp(wishlist);
        default:
            return sortByDate(wishlist);
    }
};

const sortAlphaDown = (wishlist) => {
    return wishlist.sort((a, b) => a.title.localeCompare(b.title));
};

const sortAlphaUp = (wishlist) => {
    return wishlist.sort((b, a) => a.title.localeCompare(b.title));
};

const sortBargainsUp = (wishlist) => {
    return wishlist.sort((a, b) => (a.price.inCent - a.originalPrice.inCent) - (b.price.inCent - b.originalPrice.inCent));
};

const sortByCategory = (wishlist) => {
    return wishlist.sort((a, b) => a.category.localeCompare(b.category));
};

const sortByPriceUp = (wishlist) => {
    return wishlist.sort((a, b) => a.price.inCent - b.price.inCent);
};

const sortByDate = (wishlist) => {
    return wishlist.sort((b, a) => a.addedOn - b.addedOn);
};
