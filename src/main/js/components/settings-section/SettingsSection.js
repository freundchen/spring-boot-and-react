import React from "react";
import "../../../scss/components/settingssection/SettingsSection.scss";
import LayoutSwitch from "./layout-switch/LayoutSwitch";
import SearchItems from "./text-search/SearchItems";
import SortItems from "./sort/SortItems";

const SettingsSection = ({layout, onLayoutChange, searchValue, updateSearch, sort, onSortChange}) => {

    return (
        <div className="SettingsSection">
            <SearchItems searchValue={searchValue} updateSearch={updateSearch}/>
            <SortItems sort={sort} onSortChange={onSortChange}/>
            <LayoutSwitch layout={layout} onLayoutChange={onLayoutChange}/>
        </div>
    )
};

export default SettingsSection;
