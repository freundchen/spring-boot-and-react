import React from "react";
import { StyleSheet, css } from 'aphrodite';

const SearchItems = ({searchValue, updateSearch}) => {

    const placholderText = "🔎 search for item name or category";

    return(
        <input
            type="text"
            value={searchValue}
            onChange={updateSearch}
            placeholder={placholderText}
            className={css(styles.searchInput)}
        />
    )
};

export default SearchItems;

const styles = StyleSheet.create({
    searchInput: {
        padding: '10px',
        outline: 'none',
        width: '40px',
        maxWidth: '220px',
        transition: 'width 1s',
        borderStyle: 'solid',
        borderWidth: '2px',
        borderColor: '#ccc',
        borderRadius: '25px',
        boxSizing: 'border-box',
        '::placeholder': {
            color: '#ccc',
        },
        ':focus': {
            borderColor: '#969696',
            width: '100%',
        }
    }
});

export const filteredByTextSearch = (wishlist, searchValue) => {
    return wishlist.filter(
        (item) => {
            const matchesTitle = item.title.toLowerCase().includes(searchValue.toLowerCase());
            const matchesCategory = item.category.toLowerCase().includes(searchValue.toLowerCase());
            return matchesTitle || matchesCategory;
        }
    );
};
