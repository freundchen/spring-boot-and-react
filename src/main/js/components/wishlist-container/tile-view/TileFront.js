import React from "react";
import { StyleSheet, css } from 'aphrodite';
import {formattedPrice, getPriceDropText} from "../../../utils/utils";

const TileFront = ({item}) => {

    const {image, title, category, addedOn, price, originalPrice} = item;

    const priceDropText = getPriceDropText(price.inCent, originalPrice.inCent, price.symbol, addedOn);

    return(
        <div className={css(styles.tileItemFront)}>
            <div className={css(styles.tileItemFrontImageWrapper)}>
                <img className={css(styles.tileItemFrontImage)} src={image}/>
            </div>
            <div className={css(styles.tileItemFrontInfo)}>
                <span className={css(styles.tileItemFrontInfoTitle)}>{title}</span>
                <span>{category}</span>
                <span>{priceDropText}</span>
                <span className={css(styles.tileItemFrontInfoPriceFrom)}>
                    from&nbsp;
                    <span className={css(styles.tileItemFrontInfoPrice)}>{formattedPrice(price.inCent, price.symbol)}</span>
                </span>
            </div>
        </div>
    )
};

export default TileFront;

const styles = StyleSheet.create({
    tileItemFront: {
        display: 'flex',
        flexDirection: 'column',
        justifyItems: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    tileItemFrontImage: {
        maxHeight: '160px',
        maxWidth: '160px',
    },
    tileItemFrontImageWrapper: {
        width: '160px',
        height: '160px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    tileItemFrontInfo: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        color: 'inherit',
    },
    tileItemFrontInfoTitle: {
        color: 'black',
        fontSize: '16px',
        fontWeight: 'bold',
        width: '100%',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
    },
    tileItemFrontInfoPrice: {
        color: '#ff6635',
        fontSize: '24px',
        fontWeight: 'bold',
        width: '100%',
    },
    tileItemFrontInfoPriceFrom: {
        color: '#c5c5c5',
        fontSize: '14px',
    }
});
