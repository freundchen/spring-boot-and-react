import React from "react";
import { StyleSheet, css } from 'aphrodite';
import TileFront from "./TileFront";

const TileItem = ({wishlist}) => {

    return (
        wishlist.map(item =>
            <div className={css(styles.tile)} key={item.id}>
                <TileFront item={item}/>
            </div>
        )
    );
};

export default TileItem;

const zoomKeyframes = {
    'from': {
        opacity: 0,
        transform: 'scale3d(0.3, 0.3, 0.3)'
    },

    '50%': {
        opacity: 1,
    }
};

const styles = StyleSheet.create({
    tile: {
        display: 'grid',
        gridTemplateColumns: '100%',
        gridTemplateRows: '100%',
        padding: '10px',
        backgroundColor: 'white',
        boxShadow: '0 0 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
        transition: 'all .5s cubic-bezier(.25,.8,.25,1)',
        animationDuration: '.3s',
        animationFillMode: 'both',
        animationName: zoomKeyframes,
        ':hover': {
            boxShadow: '0 3px 7px rgba(0,0,0,0.25), 0 3px 8px rgba(0,0,0,0.22)',
        },
    },
});
