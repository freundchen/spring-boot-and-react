import React from "react";
import { StyleSheet, css } from 'aphrodite';
import {formattedPrice, getPriceDropText} from "../../../utils/utils";

const ListFront = ({item}) => {

    const {image, title, category, addedOn, price, originalPrice} = item;

    const priceDropText = getPriceDropText(price.inCent, originalPrice.inCent, price.symbol, addedOn);

    return(
        <div className={css(styles.listItemFront)}>
            <div className={css(styles.listItemFrontImageWrapper)}>
                <img className={css(styles.listItemFrontImage)} src={image}/>
            </div>
            <div className={css(styles.listItemFrontInfo)}>
                <span className={css(styles.listItemFrontInfoTitle)}>{title}</span>
                <span>{category}</span>
                <span>{priceDropText}</span>
                <span className={css(styles.listItemFrontInfoPriceFrom)}>
                    from&nbsp;
                    <span className={css(styles.listItemFrontInfoPrice)}>{formattedPrice(price.inCent, price.symbol)}</span>
                </span>
            </div>
        </div>
    )
};

export default ListFront;

const styles = StyleSheet.create({
    listItemFront: {
        display: 'flex',
        flexDirection: 'row',
        justifyItems: 'center',
    },
    listItemFrontImage: {
        maxHeight: '168px',
        maxWidth: '168px',
    },
    listItemFrontImageWrapper: {
        width: '180px',
        height: '180px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    listItemFrontInfo: {
        marginLeft: '20px',
        display: 'flex',
        flexDirection: 'column',
    },
    listItemFrontInfoTitle: {
        color: 'black',
        fontSize: '16px',
        fontWeight: 'bold',
        width: '100%',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        marginRight: '30px',
    },
    listItemFrontInfoPrice: {
        color: '#ff6635',
        fontSize: '24px',
        fontWeight: 'bold',
        width: '100%',
    },
    listItemFrontInfoPriceFrom: {
        color: '#c5c5c5',
        fontSize: '14px',
    }
});
