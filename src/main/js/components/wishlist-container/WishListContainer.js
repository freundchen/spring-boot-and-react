import React from "react";
import {StyleSheet, css} from 'aphrodite';
import {LAYOUT} from "../settings-section/layout-switch/LayoutSwitch";
import ListItem from "./list-view/ListItem";
import TileItem from "./tile-view/TileItem";
import Spinner from "../spinner/Spinner";

const WishListContainer = ({wishlist, layout}) => {
    if (wishlist.length === 0) {
        return (
            <Spinner />
        );
    }

    if (layout === LAYOUT.LIST) {
        return (
            <ListItem wishlist={wishlist} />
        );
    } else {
        return (
            <div className={css(styles.tileItemWrapper)}>
                <TileItem wishlist={wishlist} />
            </div>
        );
    }
};
export default WishListContainer;

const styles = StyleSheet.create({
    tileItemWrapper: {
        display: 'grid',
        gridTemplateColumns: 'repeat(auto-fill, minmax(220px, 1fr))',
        gridTemplateRows: '330px',
        gridGap: '10px',
        gridAutoRows: '330px',
        gridAutoFlow: 'row',
        paddingBottom: '20px',
    },
});