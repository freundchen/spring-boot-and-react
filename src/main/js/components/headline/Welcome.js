import React from "react";
import { StyleSheet, minify, css } from 'aphrodite/no-important';

// using css-in-js for showcasing different way of handling components styling
// example for module used in this component: https://github.com/Khan/aphrodite
// examples of a bunch of libraries regarding that topic, here: https://github.com/MicheleBertoli/css-in-js

const Welcome = ({itemCount}) => {

    minify(false);

    return (
        <div>
            <h1 className={css(styles.headline)}>React with Spring-Boot (using kotlin) example</h1>
            <h3 className={css(styles.headlineSmall)}>You're having {itemCount} items on the list.</h3>
        </div>
    )
};
export default Welcome;

const SMALL = '@media (max-width: 600px)';

const styles = StyleSheet.create({
    headline: {
        color: '#909090',
        marginBottom: 0,
        ':hover': {
            color: 'red'
        },
        // using es6 enhanced object literals language feature to change object key dynamically
        // to have more advanced breakpoints in react a lib like react-breakpoints could help
        // https://www.npmjs.com/package/react-breakpoints
        [`${SMALL}`]: {
            color: 'green',
        }
    },
    headlineSmall: {
        color: '#a8a8a8',
        marginTop: 0,
        ':hover': {
            color: 'blue'
        }
    },
});

