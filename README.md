Spring-Boot with React playground
=================================

This project is showcasing the combination of Spring-boot and React.
The Frontend is running in Spring-Boot's embedded tomcat, Fronend resources will be bundled during the maven run


backend stack: 
* spring-boot
* kotlin
* maven

Frontend stack:
* React
* parcel
* CSS-in-JS

prerequisites
* jdk 8 or higher installed

install app

	./mvnw clean install

run app on local system
	
	./mvnw spring-boot:run